export class IoField {

  public type: string;
  public label: string;
  public fieldKey: string;
  public required: boolean;
  public value: any;

}


