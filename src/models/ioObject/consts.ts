export class Consts {

  static readonly RESERVED_OBJECT_CLASS = 'IOCLASS';
  static readonly RESERVED_ERROR_CLASS = 'IOERROR';
  static readonly RESERVED_SYSTEM_CLASS = 'IOSYSCLASS';

  static readonly RESERVED_CLASS_NAMES = [Consts.RESERVED_OBJECT_CLASS, Consts.RESERVED_ERROR_CLASS, Consts.RESERVED_SYSTEM_CLASS];

  static readonly CLASS_FIELD_CLASS = 'className';
  static readonly CLASS_FIELD_PLURAL_NAME = 'classPluralName';
  static readonly CLASS_FIELD_DESCRIPTION = 'classDescription';

  static readonly RESERVED_FIELD_NAMES = [Consts.CLASS_FIELD_CLASS, Consts.CLASS_FIELD_PLURAL_NAME, Consts.CLASS_FIELD_DESCRIPTION];

  static readonly FIELD_TYPE_TEXT = 'TEXT';
  static readonly FIELD_TYPE_NUMBER = 'NUMBER';
  static readonly FIELD_TYPE_BOOLEAN = 'BOOLEAN';
  static readonly FIELD_TYPE_DATETIME = 'DATETIME';

  static readonly FIELD_TYPES_AVAILABLE = [Consts.FIELD_TYPE_TEXT, Consts.FIELD_TYPE_NUMBER, Consts.FIELD_TYPE_BOOLEAN,
    Consts.FIELD_TYPE_DATETIME];
}
