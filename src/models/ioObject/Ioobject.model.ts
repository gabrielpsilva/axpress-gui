import { IoField } from './IoField.model';
import { Consts } from './consts';


export class IoObject {

  public id: string;
  public class: string;
  public fields: IoField[] = [];

  public static convertObjectToIoObject(objects: any[]) {

    const objArray: IoObject[] = [];

    for (const object of objects) {
      const ioo: IoObject = new IoObject();

      for (const key in object) {
        if (object.hasOwnProperty(key)) {
          ioo[key] = object[key];
        }
      }
      objArray.push(ioo);
    }
    return objArray;
  }


  fieldAdd(type: string, label: string, fieldKey: string, required: boolean, value: any) {
    let f: IoField;
    f = new IoField();

    f.type = type;
    f.label = label;
    f.fieldKey = fieldKey;
    f.required = required;
    f.value = value;
    this.fields.push(f);
  }


  fieldGet(fieldKey: string) {

    for (const field of this.fields) {
      if (field.fieldKey === fieldKey) {
        return field;
      }
    }
  }

  fieldSetValue(fieldKey: string, value: any) {

    const f = this.fieldGet(fieldKey);
    f.value = value;
  }


  fieldDelete(fieldKey: string) {

    for (const field of this.fields) {
      if (field.fieldKey === fieldKey) {
        const index = this.fields.indexOf(field, 0);
        if (index > -1) {
          this.fields.splice(index, 1);
        }
      }
    }
  }

  fieldGetExcludeClassFields() {

    const ret: IoField[] = [];

    this.fields.forEach((f) => {
      if (!Consts.RESERVED_FIELD_NAMES.includes(f.fieldKey)) {
        ret.push(f);
      }
    });
    return ret;
  }

  convertToClass(className: string, pluralName: string, description: string) {

    this.class = Consts.RESERVED_OBJECT_CLASS;

    Consts.RESERVED_CLASS_NAMES.forEach((value) => {
      this.fieldDelete(value);
    });

    let f = new IoField();
    f.type = Consts.FIELD_TYPE_TEXT;
    f.label = 'Class Name';
    f.fieldKey = Consts.CLASS_FIELD_CLASS;
    f.value = className.toUpperCase();
    f.required = true;
    this.fields.push(f);

    f = new IoField();
    f.type = Consts.FIELD_TYPE_TEXT;
    f.label = 'Plural Name';
    f.fieldKey = Consts.CLASS_FIELD_PLURAL_NAME;
    f.value = pluralName;
    f.required = true;
    this.fields.push(f);

    f = new IoField();
    f.type = Consts.FIELD_TYPE_TEXT;
    f.label = 'Description';
    f.fieldKey = Consts.CLASS_FIELD_DESCRIPTION;
    f.value = description;
    f.required = true;
    this.fields.push(f);
  }


}

