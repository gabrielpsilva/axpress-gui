import { Component, OnInit, OnDestroy } from '@angular/core';
import { IoObject } from '../../../models/ioObject/Ioobject.model';
import { Consts } from '../../../models/ioObject/consts';
import { ClassService } from '../../services/class-service';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-class-container',
  templateUrl: './class-container.component.html',
  styleUrls: ['./class-container.component.css'],
})
export class ClassItemContainerComponent implements OnInit, OnDestroy {

  private filterValue: string;
  private classesSub: Subscription;
  private isLoading: boolean;
  private classes: { object: IoObject, display: boolean }[];

  constructor(private classService: ClassService) {
    this.isLoading = true;
    this.filterValue = '';
    this.classes = [];
  }

  ngOnInit() {

    this.isLoading = true;
    this.classesSub = this.classService.allClassesSub.subscribe(
      allObjs => {
        this.classes = [];
        for (const ioObj of allObjs) {
          const pack = { object: ioObj, display: this.shouldDisplay(ioObj) };
          this.classes.push(pack);
          this.isLoading = false;
        }
      }
    );
  }

  ngOnDestroy(): void {
    this.classesSub.unsubscribe();
  }

  onFilter(event) {

    this.filterValue = event.target.value;

    for (const c of this.classes) {
      c.display = this.shouldDisplay(c.object);
    }
  }

  shouldDisplay(obj: IoObject) {

    if (this.filterValue.length <= 0) {
      return true;
    }
    const str = obj.fieldGet(Consts.CLASS_FIELD_CLASS).value;
    return str.includes(this.filterValue.toUpperCase());
  }

  onSubmit(form: NgForm) {
    const object = new IoObject();
    object.convertToClass(form.value.className, form.value.classPluralName, form.value.classDescription);
    this.isLoading = true;
    this.classService.postClass(object).subscribe({
      next: (data) => {
        this.classService.fetchAllClassses();
        this.isLoading = false;
      }
    });
  }

}
