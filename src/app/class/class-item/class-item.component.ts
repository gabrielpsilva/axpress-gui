import { Component, OnInit, Input } from '@angular/core';
import { IoObject } from '../../../models/ioObject/Ioobject.model';
import { Consts } from '../../../models/ioObject/consts';
import { IoField } from 'src/models/ioObject/IoField.model';

@Component({
  selector: 'app-class-item',
  templateUrl: './class-item.component.html',
  styleUrls: ['./class-item.component.css']
})
export class ClassItemComponent implements OnInit {

  constructor() {
  }

  @Input() ioClass: { object: IoObject, display: boolean };

  classID: string;
  className: string;
  classPlural: string;
  classDescription: string;
  display: boolean;

  fields: IoField[];


  ngOnInit() {
    this.display = this.ioClass.display;
    this.fields = this.ioClass.object.fieldGetExcludeClassFields();
    this.classID = this.ioClass.object.id;
    this.className = this.ioClass.object.fieldGet(Consts.CLASS_FIELD_CLASS).value;
    this.classPlural = this.ioClass.object.fieldGet(Consts.CLASS_FIELD_PLURAL_NAME).value;
    this.classDescription = this.ioClass.object.fieldGet(Consts.CLASS_FIELD_DESCRIPTION).value;
  }

}
