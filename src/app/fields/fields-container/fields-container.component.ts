import { Component, OnInit, OnDestroy } from '@angular/core';
import { ClassService } from '../../services/class-service';
import { ActivatedRoute } from '@angular/router';
import { Consts } from '../../../models/ioObject/consts';
import { Subscription } from 'rxjs';
import { NgForm } from '@angular/forms';
import { IoObject } from 'src/models/ioObject/Ioobject.model';

@Component({
  selector: 'app-fields-container',
  templateUrl: './fields-container.component.html',
  styleUrls: ['./fields-container.component.css']
})
export class FieldsContainerComponent implements OnInit, OnDestroy {

  // Form mapping
  classID: string;
  className: string;
  classPlural: string;
  classDescription: string;
  fieldRequired: string;
  fieldType: string;

  // IoObject of this page
  object: IoObject;

  fields: any[]; // can not call this.object from template (workaround)

  private classesSub: Subscription;

  constructor(private classService: ClassService, private route: ActivatedRoute) {
    this.fieldRequired = 'NO';
    this.fieldType = 'TEXT';
  }

  ngOnInit() {

    const idStr = this.route.snapshot.params.id;

    this.classesSub = this.classService.allClassesSub.subscribe({
      next: allObjs => {
        for (const o of allObjs) {
          if (o.id === idStr) {
            this.object = o;
            this.fields = this.object.fieldGetExcludeClassFields();
            this.classID = this.object.id;
            this.className = this.object.fieldGet(Consts.CLASS_FIELD_CLASS).value;
            this.classPlural = this.object.fieldGet(Consts.CLASS_FIELD_PLURAL_NAME).value;
            this.classDescription = this.object.fieldGet(Consts.CLASS_FIELD_DESCRIPTION).value;
            break;
          }
        }
      },
    });

  }

  onSubmitProperties(form: NgForm) {

    // store mongodb update statement
    const updateJson = [];
    // push filter
    updateJson.push({
      class: this.object.class,
      'fields.value': this.object.fieldGet(Consts.CLASS_FIELD_CLASS).value
    });

    // set values according to the form
    this.object.fieldSetValue(Consts.CLASS_FIELD_CLASS, form.value.className);
    this.object.fieldSetValue(Consts.CLASS_FIELD_PLURAL_NAME, form.value.classPlural);
    this.object.fieldSetValue(Consts.CLASS_FIELD_DESCRIPTION, form.value.classDescription);

    const req = (this.fieldRequired === 'YES' ? true : false);
    this.object.fieldAdd(this.fieldType, form.value.labelField, form.value.labelField, req, null);

    // push value to set
    updateJson.push({ $set: this.object });

    // Submit mongo update statement
    this.classService.patchClass(updateJson).subscribe({
      next: (data) => {

        // update variables binded to UI
        this.className = this.object.fieldGet(Consts.CLASS_FIELD_CLASS).value;
        this.classPlural = this.object.fieldGet(Consts.CLASS_FIELD_PLURAL_NAME).value;
        this.classDescription = this.object.fieldGet(Consts.CLASS_FIELD_DESCRIPTION).value;

        this.fields = this.object.fieldGetExcludeClassFields();
      }
    });
  }

  ngOnDestroy(): void {
    this.classesSub.unsubscribe();
  }

  OnDeleteField(fieldKeyEle: HTMLElement) {

    // store mongodb update statement
    const updateJson = [];
    // push filter
    updateJson.push({
      class: this.object.class,
      'fields.value': this.object.fieldGet(Consts.CLASS_FIELD_CLASS).value
    });

    this.object.fieldDelete(fieldKeyEle.innerText);

    // push value to set
    updateJson.push({ $set: this.object });

    // Submit mongo update statement
    this.classService.patchClass(updateJson).subscribe({
      next: (data) => {

        this.fields = this.object.fieldGetExcludeClassFields();
      }
    });
  }
}
