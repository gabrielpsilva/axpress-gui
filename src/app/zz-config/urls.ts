export class Urls {

  static readonly DOMAIN_URL = 'https://axpress-gs4lfxihta-uc.a.run.app/';
  //static readonly DOMAIN_URL = 'http://localhost:8080/';


  static readonly V1_OBJECTS_URL = Urls.DOMAIN_URL + 'v1/objects/';
  static readonly V1_OBJECTS_QUERY_URL = Urls.V1_OBJECTS_URL + 'query';
}
