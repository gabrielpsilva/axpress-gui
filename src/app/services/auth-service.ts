import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {delay, tap} from 'rxjs/operators';
import {User} from '../../models/user/user.model';
import {BehaviorSubject} from 'rxjs';
import {Router} from '@angular/router';

export interface AuthResponseData {
  kind: string;
  idToken: string;
  email: string;
  refreshToken: string;
  expiresIn: string;
  localId: string;
  registered?: boolean;
}

@Injectable({providedIn: 'root'})
export class AuthService {

  public user = new BehaviorSubject<User>(null);

  constructor(private http: HttpClient, private router: Router) {
  }

  login(emailParam: string, passwordParam: string){
    const url = 'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=' +
      'AIzaSyDwlMAqRytsaCbclEu-frZZWJAN-Xvdzx4';

    return  this.http.post<AuthResponseData>(url, {
      email: emailParam,
      password: passwordParam,
      returnSecureToken: true
    })
      .pipe(
        delay(0),
        tap(resData => {
          this.handleAuthentication(resData.email, resData.localId, resData.idToken, +resData.expiresIn );
        }),
      );
  }

  logout() {
    this.user.next(null);
    this.router.navigate(['/']);
  }

  handleAuthentication(email: string, userId: string, token: string, expiresIn: number) {
    const expirationDate = new Date(new Date().getTime() + expiresIn * 1000);
    const user = new User(email, userId, token, expirationDate);
    this.user.next(user);
  }
}
