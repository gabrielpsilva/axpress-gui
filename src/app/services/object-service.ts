import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { IoObject } from '../../models/ioObject/Ioobject.model';
import { Urls } from '../zz-config/urls';
import { Consts } from '../../models/ioObject/consts';
import { delay, map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
  })
export class ObjectService {

    public allObjectsSub: BehaviorSubject<IoObject[]>;


  constructor(private http: HttpClient) {
    this.allObjectsSub = new BehaviorSubject([]);
    this.fetchAllObjects();
  }

  fetchAllObjects() {
      this.http.post<IoObject[]>(Urls.V1_OBJECTS_QUERY_URL, `{ "class": { "$not": { "$regex": "^IOCLASS.*" } } }`)
        .pipe(
          delay(0),
          map(data => data)
        )
        .subscribe(
          resp => {
              const conv = IoObject.convertObjectToIoObject(resp);
              this.allObjectsSub.next(conv);
          },
          err => {
            console.log(err);
          }
        );
  }
}


