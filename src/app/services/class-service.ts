import { IoObject } from '../../models/ioObject/Ioobject.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Urls } from '../zz-config/urls';
import { Consts } from '../../models/ioObject/consts';
import { map, delay } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';


@Injectable({providedIn: 'root'})
export class ClassService {

  public allClassesSub: BehaviorSubject<IoObject[]>;

  constructor(private http: HttpClient) {
    this.allClassesSub = new BehaviorSubject([]);
    this.fetchAllClassses();
  }


  fetchAllClassses() {
    console.log('fetchAllClassses');
    this.http.post<IoObject[]>(Urls.V1_OBJECTS_QUERY_URL, `{"class": "${Consts.RESERVED_OBJECT_CLASS}"}`)
      .pipe(
        delay(0),
        map(data => data)
      )
      .subscribe(
        resp => {
          const conv = IoObject.convertObjectToIoObject(resp);
          this.allClassesSub.next(conv);
        },
        err => {
          console.log(err);
        }
      );
  }


  getClassByID(id: string) {

    if (this.allClassesSub.value.length <= 0) {
      this.fetchAllClassses();
    }

    let ret: IoObject = new IoObject();
    const classItems = this.allClassesSub.value;

    classItems.forEach((c) => {
      if (c.id === id) {
        ret = c;
      }
    });
    return ret;
  }

  postClass(classObj: IoObject) {
    const asArray = [];
    asArray.push(classObj);

    return this.http.post(Urls.V1_OBJECTS_URL, asArray, {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    }).pipe(
      delay(0),
      map(data => data)
    );
  }

  patchClass(updateObj: any) {

    return this.http.patch(Urls.V1_OBJECTS_URL, updateObj, {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    }).pipe(
      delay(0),
      map(data => data)
    );
  }

}
