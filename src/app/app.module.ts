import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'
import { RouterModule, Routes } from '@angular/router';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { ClassItemComponent } from './class/class-item/class-item.component';
import { ClassItemContainerComponent } from './class/class-container/class-container.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';

import { HomeComponent } from './home/home.component';
import { FieldsContainerComponent } from './fields/fields-container/fields-container.component';

import { ClassService } from './services/class-service';
import { ObjectContainerComponent } from './object/object-container/object-container.component';
import { LoginComponent } from './login/login.component';
import {AuthInterceptorService} from './services/auth-interceptor-service';
import {AuthGuard} from './services/auth.guard';

const appRoutes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'class', component: ClassItemContainerComponent, children: [], canActivate: [AuthGuard]},
  { path: 'class/:id', component: FieldsContainerComponent, canActivate: [AuthGuard] },
  { path: 'object', component: ObjectContainerComponent, canActivate: [AuthGuard] }
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ClassItemComponent,
    ClassItemContainerComponent,
    NavBarComponent,
    HomeComponent,
    FieldsContainerComponent,
    ObjectContainerComponent,
    LoginComponent,

  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [{provide: HTTP_INTERCEPTORS, useClass: AuthInterceptorService, multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule { }
