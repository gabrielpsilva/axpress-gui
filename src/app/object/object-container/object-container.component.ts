import { Component, OnInit, OnDestroy } from '@angular/core';
import { ClassService } from '../../services/class-service';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';
import { IoObject } from '../../../models/ioObject/Ioobject.model';
import { Consts } from '../../../models/ioObject/consts';
import { IoField } from '../../../models/ioObject/IoField.model';
import { ObjectService } from '../../services/object-service';

@Component({
  selector: 'app-object-container',
  templateUrl: './object-container.component.html',
  styleUrls: ['./object-container.component.css']
})

export class ObjectContainerComponent implements OnInit, OnDestroy {


  private classesSub: Subscription;
  private isLoading: boolean;
  private ioClasses: IoObject[];
  private ioObjects: IoObject[];
  private selectedClass: IoObject;
  //private selectedClassFields: IoField[];

  constructor(private classService: ClassService, private objectService: ObjectService) {
    this.selectedClass = new IoObject();
  }

  ngOnInit() {

    this.isLoading = true;

    this.classesSub = this.classService.allClassesSub.subscribe({
      next: allObjs => {
        this.ioClasses = allObjs;
      }
    });

    this.classesSub = this.objectService.allObjectsSub.subscribe({
      next: allObjs => {
        this.ioObjects = allObjs;
      }
    });
  }

  ngOnDestroy() {
  }

  onClassChange(selected: string) {

    for (const c of this.ioClasses) {
      if (c.fieldGet(Consts.CLASS_FIELD_CLASS).value === selected) {
        this.selectedClass = c;
        console.log('selected', this.selectedClass);
        break;
      }
    }
  }

  onSubmit(form: NgForm) {

    const newObject = new IoObject();

    newObject.class = this.selectedClass.fieldGet(Consts.CLASS_FIELD_CLASS).value;
    newObject.fields = this.selectedClass.fieldGetExcludeClassFields();

    Object.keys(form.value).forEach(key => {
      newObject.fieldSetValue(key, form.value[key]);
    });

    console.log(newObject);
    // this.selectedClass = new IoObject();

    this.classService.postClass(newObject).subscribe({
      next: (data) => {
        this.objectService.fetchAllObjects();
        this.isLoading = false;
      }
    });
  }

}
