import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {AuthService} from '../services/auth-service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  private isLoading = false;

  constructor(private http: HttpClient, private authService: AuthService, private router: Router) {
  }

  ngOnInit() {
  }

  onSubmit(form: NgForm) {
  if (!form.valid) {
    return;
  }

  const login = form.value.email;
  const password = form.value.password;

  this.isLoading = true;
  this.authService.login(login, password).subscribe({
    next: resData => {
      this.isLoading = false;
      this.router.navigate(['/object']);
    },
    error: errData => {
      this.isLoading = false;
    }
  })

  form.reset();

  }
}
